@login
Feature: Login
  As a PO
  I want to login to application
  So that verify login function work well

  Scenario: Login to system with param
    Given I open application
    When I input to Username "mngr163633"
    And I input to Password "hEjApuz"
    And I click to Login button
    Then Verify Home page displayed with message "Welcome To Manager's Page of Guru99 Bank"
    And I close browser

  Scenario Outline: Login to system with Example datatable
    Given I open application
    When I input to Username "<Username>"
    And I input to Password "<Password>"
    And I click to Login button
    Then Verify Home page displayed with message "Welcome To Manager's Page of Guru99 Bank"
    And I close browser

    Examples: Username and Password Datatable
      | Username   | Password |
      | mngr163633 | hEjApuz  |
      | mngr164840 | ugAhUdU  |
      | mngr164841 | hapEsyr  |