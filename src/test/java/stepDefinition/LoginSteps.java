package stepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class LoginSteps {
    WebDriver driver;
    @Given("^I open application$")
    public void iOpenApplication()  {
        driver = new FirefoxDriver();
        driver.get("http://demo.guru99.com/v4");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @When("^I input to Username \"([^\"]*)\"$")
    public void iInputToUsername(String userName)  {
        driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(userName);


    }

    @When("^I input to Password \"([^\"]*)\"$")
    public void iInputToPassword(String passWord)  {
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(passWord);


    }

    @When("^I click to Login button$")
    public void iClickToLoginButton()  {
        driver.findElement(By.xpath("//input[@name='btnLogin']")).click();

    }

    @Then("^Verify Home page displayed with message \"([^\"]*)\"$")
    public void verifyHomePageDisplayedWithMessage(String messageName)  {

        Assert.assertTrue(driver.findElement(By.xpath("//marquee[text()=\""+ messageName +"\"]")).isDisplayed());
    }

    @Then("^I close browser$")
    public void iCloseBrowser()  {
        driver.quit();

    }

}
