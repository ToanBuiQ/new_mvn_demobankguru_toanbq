package actions;

import commons.AbstractPage;
import org.openqa.selenium.WebDriver;
import interfaces.NewCustomerPageUI;

public class NewCustomerPageObject extends AbstractPage {
    WebDriver driver;

    public NewCustomerPageObject(WebDriver driver) {
        this.driver = driver;
    }

    public WithdrawPageObject openWithdrawPage() {
        waitForControlVisible(driver, NewCustomerPageUI.WITHDRAW_LINK);
        clickToElement(driver, NewCustomerPageUI.WITHDRAW_LINK);
        return PageFactoryManager.openWithdrawPageObject(driver);
    }

    public HomePageObject clickSubmitButton() {
        waitForControlVisible(driver, NewCustomerPageUI.SUBMIT_BUTTON);
        clickToElement(driver, NewCustomerPageUI.SUBMIT_BUTTON);
        return PageFactoryManager.openHomePageObject(driver);
    }

    public String getCustomerID() {
        waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_ID);
        return getText(driver, NewCustomerPageUI.CUSTOMER_ID);
    }
}
