package actions;

import commons.AbstractPage;
import org.openqa.selenium.WebDriver;
import interfaces.RegisterPageUI;

public class RegisterPageObject extends AbstractPage {
    WebDriver driver;

    public RegisterPageObject(WebDriver driver) {
        this.driver = driver;
    }

    public void senkeyToEmailTextbox(String email) {
        waitForControlVisible(driver, RegisterPageUI.EMAIL_TEXTBOX);
        senkeysToElement(driver, RegisterPageUI.EMAIL_TEXTBOX, email);
    }

    public void clickSubmitButton() {
        waitForControlVisible(driver, RegisterPageUI.SUBMIT_BUTTON);
        clickToElement(driver, RegisterPageUI.SUBMIT_BUTTON);
    }

    public String getUserID() {
        waitForControlVisible(driver, RegisterPageUI.USER_ID);
        return getText(driver, RegisterPageUI.USER_ID);
    }

    public String getPassword() {
        waitForControlVisible(driver, RegisterPageUI.PASSWORD);
        return getText(driver, RegisterPageUI.PASSWORD);
    }

}
