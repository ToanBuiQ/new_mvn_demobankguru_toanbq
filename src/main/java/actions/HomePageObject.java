package actions;

import commons.AbstractPage;
import org.openqa.selenium.WebDriver;
import interfaces.HomePageUI;

public class HomePageObject extends AbstractPage {

    WebDriver driver;

    public HomePageObject(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isHomePageDisplayed() {
        waitForControlVisible(driver, HomePageUI.WELCOME_MESSAGES);
        return isControlDisplayed(driver, HomePageUI.WELCOME_MESSAGES);
    }


}
