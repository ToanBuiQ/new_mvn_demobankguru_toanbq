package commons;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class AbstractTest {
    WebDriver driver;
    protected final Log log;

    protected AbstractTest() {
        log = LogFactory.getLog(getClass());
    }

    public WebDriver openMultiBrower(String browerName, String url) {
        if (browerName.equals("chrome")) {
            System.getProperty("webdriver.chrome.driver", ".//resources/chromedriver");
            driver = new ChromeDriver();
            driver.manage().window().fullscreen();
        } else if (browerName.equals("firefox")) {
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
        } else {
            System.getProperty("webdriver.chrome.driver", ".//resources/chromedriver");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("headless");
            options.addArguments("window-size=1379x768");
            driver = new ChromeDriver(options);
        }
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public int randomNumber() {
        Random random = new Random();
        int number = random.nextInt(9999) + 1;
        return number;
    }

    public void closeBrower(WebDriver driver) {
        try {
            String osName = System.getProperty("os.name").toLowerCase();
            String cmd = "";
            driver.quit();
            if (driver.toString().toLowerCase().contains("chrome")) {
                if (osName.contains("mac")) {
                    cmd = "pkill chromedriver";
                } else cmd = "taskkill /F /FI eq chromedriver*\"";
                Runtime.getRuntime().exec(cmd).waitFor();
            }
            if (driver.toString().toLowerCase().contains("gecko")) {
                if (osName.contains("mac")) {
                    cmd = "pkill geckodriver";
                } else cmd = "taskkill /F /FI eq geckodriver*\"";
                Runtime.getRuntime().exec(cmd).waitFor();
            }

            if (driver.toString().toLowerCase().contains("internetexplorer")) {
                cmd = "taskkill /F /FI eq IEDriverServer*\"";
                Runtime.getRuntime().exec(cmd).waitFor();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

