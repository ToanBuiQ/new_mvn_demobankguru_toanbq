package commons;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import actions.*;
import interfaces.AbstractPageUI;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class AbstractPage {

    // Brower

    public void openAnyUrl(WebDriver driver, String url) {
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public String getPageTitle(WebDriver driver) {
        return driver.getTitle();
    }

    public String getPageURL(WebDriver driver) {
        return driver.getCurrentUrl();
    }

    public void backToPreviousPage(WebDriver driver) {
        driver.navigate().back();
    }

    public void forwardToNextPage(WebDriver driver) {
        driver.navigate().forward();
    }

    public void refreshPage(WebDriver driver) {
        driver.navigate().refresh();
    }

    public void quitBrower(WebDriver driver) {
        driver.quit();
    }

    // WebElement
    public void clickToElement(WebDriver driver, String locator) {
        waitForControlVisible(driver, locator);
        driver.findElement(By.xpath(locator)).click();
    }

    public void clickToElement(WebDriver driver, String locator, String value) {
        locator = String.format(locator, value);
        waitForControlVisible(driver, locator);
        driver.findElement(By.xpath(locator)).click();
    }

    public void senkeysToElement(WebDriver driver, String locator, String value) {
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();
        element.sendKeys(value);
    }

    public void senkeysToElement(WebDriver driver, String locator, String value, String... inputDynamicID) {
        locator = String.format(locator, (Object[]) inputDynamicID);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void pressKeyForElement(WebDriver driver, String locator, Keys keyname) {
        WebElement element = driver.findElement(By.xpath(locator));
        Actions action = new Actions(driver);
        action.sendKeys(element, keyname);
    }

    public void pressTAB(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();
        element.sendKeys(Keys.TAB);
    }

    public void pressTAB(WebDriver driver, String locator, String... inputDynamicID) {
        locator = String.format(locator, (Object[]) inputDynamicID);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(Keys.TAB);
    }

    public void selectItemInDropDown(WebDriver driver, String locator, String text) {
        Select select = new Select(driver.findElement(By.xpath(locator)));
        select.selectByVisibleText(text);
    }

    public void selectItemInDropDown(WebDriver driver, String locator, String text, String... inputDynamicName) {
        locator = String.format(locator, (Object[]) inputDynamicName);
        Select select = new Select(driver.findElement(By.xpath(locator)));
        select.selectByVisibleText(text);
    }

    public void selectCustomDropdown(WebDriver driver, String dropdown, String listItem, String Value)
            throws Exception {
        WebElement dropdownElement = driver.findElement(By.xpath(dropdown));
        WebDriverWait wait = new WebDriverWait(driver, 30);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dropdownElement);
        dropdownElement.click();
        List<WebElement> allItems = driver.findElements(By.xpath(listItem));
        wait.until(ExpectedConditions.visibilityOfAllElements(allItems));
        for (WebElement item : allItems) {
            if (item.getText().equals(Value)) {
                // scroll to item
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", item);
                item.isDisplayed();
                item.click();
                Thread.sleep(2000);
                break;
            }
        }
    }

    public String getFirstItemSelected(WebDriver driver, String locator) {
        Select select = new Select(driver.findElement(By.xpath(locator)));
        return select.getFirstSelectedOption().getText();
    }

    public String getAttributeValue(WebDriver driver, String locator, String attributeName) {
        WebElement element = driver.findElement(By.xpath(locator));
        return element.getAttribute(attributeName);
    }

    public String getText(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        return element.getText();
    }

    public String getText(WebDriver driver, String locator, String value) {
        locator = String.format(locator, value);
        WebElement element = driver.findElement(By.xpath(locator));
        return element.getText();
    }

    public int getSizeElement(WebDriver driver, String locator) {
        List<WebElement> elements = driver.findElements(By.xpath(locator));
        return elements.size();
    }

    public void checkToChecbox(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        if (!element.isSelected()) {
            element.click();
        }
    }

    public void uncheckToChecbox(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        if (element.isSelected()) {
            element.click();
        }
    }

    public boolean isControlDisplayed(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        return element.isDisplayed();
    }

    public boolean isControlDisplayed(WebDriver driver, String locator, String value) {
        locator = String.format(locator, value);
        WebElement element = driver.findElement(By.xpath(locator));
        return element.isDisplayed();
    }

    public boolean isControlUnDisplayed(WebDriver driver, String locator) {
        List<WebElement> elements = driver.findElements(By.xpath(locator));
        overrideTimeout(driver, 5);

        if (elements.size() == 0) {
            overrideTimeout(driver, 30);
            return true;
        } else {
            overrideTimeout(driver, 30);
            return false;
        }
    }

    public boolean isControlUnDisplayed(WebDriver driver, String locator, String value) {
        locator = String.format(locator, value);
        List<WebElement> elements = driver.findElements(By.xpath(locator));
        overrideTimeout(driver, 5);

        if (elements.size() == 0) {
            overrideTimeout(driver, 30);
            return true;
        } else {
            overrideTimeout(driver, 30);
            return false;
        }
    }

    public boolean isControlSelected(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        return element.isSelected();
    }

    public boolean isControlEnabled(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        return element.isEnabled();
    }

    // Alert
    public void acceptAlert(WebDriver driver) {
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    public void dissmisAlert(WebDriver driver) {
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
    }

    public String getTextToAlert(WebDriver driver) {
        Alert alert = driver.switchTo().alert();
        return alert.getText();
    }

    public void senKeyToAlert(WebDriver driver, String text) {
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(text);
    }

    // Windows
    public void switchToWindowByUDID(WebDriver driver, String parentUDID) {
        Set<String> allWindows = driver.getWindowHandles();
        for (String runWindow : allWindows) {
            if (!runWindow.equals(parentUDID)) {
                driver.switchTo().window(runWindow);
                break;
            }
        }
    }

    public void switchToWindowByTitle(WebDriver driver, String title) {
        Set<String> allWindows = driver.getWindowHandles();
        for (String runWindows : allWindows) {
            driver.switchTo().window(runWindows);
            String currentWindow = driver.getTitle();
            if (currentWindow.equals(title)) {
                break;
            }
        }
    }

    public void closeToWindow(WebDriver driver, String UDID) {
        Set<String> allWindow = driver.getWindowHandles();
        for (String runWindow : allWindow) {
            if (!runWindow.equals(UDID)) {
                driver.switchTo().window(runWindow);
                driver.close();
            }
        }
        driver.switchTo().window(UDID);
    }

    public void switchToIframe(WebDriver driver, String locator) {
        driver.switchTo().frame(locator);
    }

    public void doubleClickToElement(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        Actions actions = new Actions(driver);
        actions.doubleClick(element).perform();
    }

    public void hoverMouseToElement(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
    }

    public void rightClickToElement(WebDriver driver, String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        Actions action = new Actions(driver);
        action.contextClick(element).perform();
    }

    public void drapAndDropElement(WebDriver driver, String source, String target) {
        WebElement startPoint = driver.findElement(By.xpath(source));
        WebElement endPoint = driver.findElement(By.xpath(target));
        Actions action = new Actions(driver);
        action.dragAndDrop(startPoint, endPoint).perform();
    }

    public void uploadFile(WebDriver driver, String fileName) {
        String proDir = System.getProperty("user.dir");
        String filePath = proDir + "/resources/" + fileName;
        WebElement element = driver.findElement(By.xpath("//input[@type='file']"));
        element.sendKeys(filePath);
    }

    public void upLoadMultiFile(WebDriver driver, String[] fileNames) {
        String proDir = System.getProperty("user.dir");
        WebElement element = driver.findElement(By.xpath("//input[@type='file']"));
        for (String string : fileNames) {
            String filePath = proDir + "/resources/" + string;
            element.sendKeys(filePath);
        }
    }

    public void openByJS(WebDriver driver, String url) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.location =" + url + ";");
    }

    public void clickToElementByJS(WebDriver driver, String locator) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.xpath(locator));
        js.executeScript("arguments[0].click();", element);
    }

    public void scrollToButtonPage(WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");

    }

    public void removeAttributeInDOM(WebDriver driver, String locator, String attribute) {
        WebElement element = driver.findElement(By.xpath(locator));
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void waitForControlPresence(WebDriver driver, String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
    }

    public void waitForControlVisible(WebDriver driver, String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    public void waitForControlVisible(WebDriver driver, String locator, String value) {
        locator = String.format(locator, value);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    public void waitForControlClickable(WebDriver driver, String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
    }

    public void waitForControlInvisible(WebDriver driver, String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
    }

    public void waitForControlAlertPresence(WebDriver driver, String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.alertIsPresent());
    }

    public HomePageObject openHomePage(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Manager");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Manager");
        return PageFactoryManager.openHomePageObject(driver);
    }

    public NewCustomerPageObject openNewCustomerPage(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "New Customer");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "New Customer");
        return PageFactoryManager.openNewCustomerPageObject(driver);
    }

    public WithdrawPageObject openWithdrawPage(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Withdrawal");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Withdrawal");
        return PageFactoryManager.openWithdrawPageObject(driver);
    }

    public EditCustomerPageObject openEditCustomerPage(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Edit Customer");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Edit Customer");
        return PageFactoryManager.openEditCustomerPageObject(driver);
    }

    public NewAccountPageObject openNewAccountPage(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "New Account");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "New Account");
        return PageFactoryManager.openNewAccountPageObject(driver);
    }

    public DepositPageObject openDepositPage(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Deposit");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Deposit");
        return PageFactoryManager.openDepositPageObject(driver);
    }

    public FundTranferPageObject openFundTranferPage(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Fund Transfer");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Fund Transfer");
        return PageFactoryManager.openFundTranferPageObject(driver);
    }

    public BalanceEnquiryPageObject openBalanceEnquiryPageObject(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Balance Enquiry");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Balance Enquiry");
        return PageFactoryManager.openBalanceEnquiryPageObject(driver);
    }

    public DeleteAccountPageObject openDeleteAccountPageObject(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Delete Account");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Delete Account");
        return PageFactoryManager.openDeleteAccountPageObject(driver);
    }

    public DeleteCustomerPageObject openDeleteCustomerPageObject(WebDriver driver) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Delete Customer");
        clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, "Delete Customer");
        return PageFactoryManager.openDeleteCustomerPageObject(driver);
    }



    public void overrideTimeout(WebDriver driver, long timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public void inputToTextbox(WebDriver driver, String inputDynamicID, String value) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTBOX_BUTTON, inputDynamicID);
        senkeysToElement(driver, AbstractPageUI.DYNAMIC_TEXTBOX_BUTTON, value, inputDynamicID);
    }

    public void clickToButton(WebDriver driver, String inputDynamicName) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTBOX_BUTTON, inputDynamicName);
        clickToElement(driver, AbstractPageUI.DYNAMIC_TEXTBOX_BUTTON, inputDynamicName);
    }

    public void inputToAdress(WebDriver driver, String value) {
        waitForControlVisible(driver, AbstractPageUI.ADDRESS_TEXTAREA);
        senkeysToElement(driver, AbstractPageUI.ADDRESS_TEXTAREA, value);
    }

    public void selectItemDropDown(WebDriver driver, String text, String inputDynamicName) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_DROPBOX, inputDynamicName);
        selectItemInDropDown(driver, AbstractPageUI.DYNAMIC_DROPBOX, text, inputDynamicName);
    }

    public String getTextFromTable(WebDriver driver, String inputDynamicText) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TABLE, inputDynamicText);
        return getText(driver, AbstractPageUI.DYNAMIC_TABLE, inputDynamicText);
    }

    public boolean isMessageDisplayed(WebDriver driver, String inputDynamicText) {
        waitForControlVisible(driver, AbstractPageUI.DYNAMIC_CONFIRM_MESSAGES, inputDynamicText);
        return isControlDisplayed(driver, AbstractPageUI.DYNAMIC_CONFIRM_MESSAGES, inputDynamicText);
    }

}
