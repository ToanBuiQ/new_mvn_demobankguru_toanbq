package interfaces;

public class AbstractPageUI {
    public static final String DYNAMIC_PAGE_LINK = "//a[text()='%s']";
    public static final String DYNAMIC_TEXTBOX_BUTTON = "//input[@name='%s']";
    public static final String ADDRESS_TEXTAREA = "//textarea[@name='addr']";
    public static final String DYNAMIC_DROPBOX = "//select[@name='%s']";
    public static final String DYNAMIC_TABLE = "//td[text()='%s']//following-sibling::td";
    public static final String DYNAMIC_CONFIRM_MESSAGES = "//p[text()='%s']";


}
