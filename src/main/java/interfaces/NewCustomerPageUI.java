package interfaces;

public class NewCustomerPageUI {
    public static final String WITHDRAW_LINK = "//a[text()='Withdrawal']";
    public static final String SUBMIT_BUTTON = "//input[@name='sub']";
    public static final String CUSTOMER_ID = "//td[text()='Customer ID']//following-sibling::td";
}
